var mysql       = require('../modules/mysql.js');
var logger      = require('../modules/Logger.js');

var jwt         = require('jsonwebtoken');

function Registry(app, path) {

    // path: '/registry'

    /* COMUNITIES */
    app.get(path + '/comunities', function(req, res, next) {
        logger.info("- Getting Comunities -");
        
        mysql.query('SELECT * FROM comunities;',function(err, rows) {
        if(err) { logger.error(err); res.json({err: 1}); return; }

        res.json(rows);
        });
    });


    /* PROVINCES */
    app.get(path + '/provinces', function(req, res, next) {
        logger.info("- Getting Provinces -");

        var comunity_id = req.query.comunity_id;
        
        mysql.query('SELECT * FROM provinces WHERE comunity_id=' + comunity_id + ';',function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            res.json(rows);
        });
    });



    /* VILLAGES */
    app.get(path + '/villages', function(req, res, next) {
        logger.info("- Getting Villages -");

        var province_id = req.query.province_id;
        
        mysql.query('SELECT * FROM villages WHERE province_id=' + province_id + ';',function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            res.json(rows);
        });
    });


    // Checks the User Name data exist on BBDD.
    // REQUEST:
    // body:    - userName
    // RESPONSE:
    // { 
    //      exist:  0: No exist userName on the BBDD. || 1: Exist userName on the BBDD.
    //      err:    0: No error. || 1: Error on query. || 2: userName field on request is undefined.
    //}
    app.get(path + '/check/userName', function(req, res, next) {
        logger.info("Checking User Name field");

        var userName    = req.query.userName;
        if(userName === '') { res.json({err: 2}); return;}

        mysql.query('SELECT * FROM users WHERE user_name="' + userName + '";', function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            if(rows.length == 0)    { res.json({exist: 0}); }
            else                    { res.json({exist: 1}); }
        });
    });


    // Checks the Email data exist on BBDD.
    // REQUEST:
    // body:    - email
    // RESPONSE:
    // { 
    //      exist:  0: No exist email on the BBDD. || 1: Exist email on the BBDD.
    //      err:    0: No error. || 1: Error on query. || 2: email field on request is undefined.
    //}
    app.get(path + '/check/email', function(req, res, next) {
        logger.info("Checking Email field");

        var email   = req.query.email;
        if(email === '') { res.json({err: 2}); }

        mysql.query('SELECT * FROM users WHERE email="' + email + '";', function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            if(rows.length == 0)    { res.json({exist: 0}); }
            else                    { res.json({exist: 1}); }
        });
    });


    app.post(path + '/signup', function(req, res, next) {
        logger.info("Creating one new user.");
        var user = req.body.data.user;
        var personal = req.body.data.personal;
        var address = req.body.data.address;

        mysql.query('INSERT INTO users (`user_name`, `email`, `password`, `name`, `surname`, `second_surname`, `birthday`, `signup_date`) VALUES ("'
                     + user.userName + '", "' + user.email + '", "' + user.password + '", "' + personal.name + '", "' + personal.surname + '", "' + personal.secondSurname + '", "' + personal.birthDate + '", NOW());', function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            if(rows.length == 0)    { res.json({exist: 0}); }
            else                    { res.json({exist: 1}); }
        });

    });

    app.post(path + '/login', function(req, res, next) {
        logger.info("Login user with email and password.");
        logger.info(req.body.data);

        var log = req.body.data;

        mysql.query('SELECT * FROM users WHERE email="' + log.email + '";', function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            if(rows.length == 0)    { res.json({err: 1, email: 0, password: 0}); }
            else {
                mysql.query('SELECT * FROM users WHERE email="' + log.email + '" AND `password`="' + log.password + '";', function(err, rows) {
                    logger.info("On password");
                    if(err) { logger.error(err); res.json({err: 1}); return; }

                    var token = jwt.sign(log, app.get('superSecret'), {
                        expiresIn: '24h'
                    });

                    if(rows.length == 0)    { res.json({err: 1, email: 1, password: 0}); }
                    else                    { res.json({err: 0, token: token}); }
                });
            }
        });
    });
}

module.exports = Registry;