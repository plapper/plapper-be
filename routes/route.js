var registryRoute = require('./registry.route.js');
var servicesRoute = require('./services.route.js');
var workersRoute  = require('./workers.route.js');

var Token       = require('../modules/token.js');


module.exports = function Route(app) {

    var token = new Token(app);

    // OPEN REST.
    this.registry = registryRoute(app, '/registry');

    // PRIVATE REST.
    this.services = servicesRoute(app, token.validation, '/api/services');
    this.workers  = workersRoute(app, token.validation, '/api/workers');

}