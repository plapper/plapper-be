var mysql       = require('../modules/mysql.js');
var logger      = require('../modules/Logger.js');

function Workers(app, validation, path) {

    /* CATEGORIES */
    app.get(path + '/workers', validation, function(req, res, next) {
        logger.debug("- Getting workers -");

        var categoryId = req.query.categoryId;
        
        mysql.query('SELECT id, name, stars, open_year FROM workers WHERE category_id=' + categoryId + ';',function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            res.json(rows);
        });
    });

}

module.exports = Workers;