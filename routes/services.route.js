var mysql       = require('../modules/mysql.js');
var logger      = require('../modules/Logger.js');

function Services(app, validation, path) {

    /* CATEGORIES */
    app.get(path + '/categories', validation, function(req, res, next) {
        logger.debug("- Getting Categories -");
        
        mysql.query('SELECT * FROM categories;',function(err, rows) {
            if(err) { logger.error(err); res.json({err: 1}); return; }

            res.json(rows);
        });
    });

}

module.exports = Services;