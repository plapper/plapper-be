# DOCKER INSTALLATION

Installation of docker on **Raspberry Pi 3** to install all docker development images.
First download code and install it.

```bash
curl -sSL https://get.docker.com | sh
```

Give sudo permissions to docker user

```bash
sudo usermod -aG docker $USER
```

Reload bash terminal to apply changes.

# DEVELOPMENT SEVER INSTALLATION
Steps to install one development server with one BBDD node and API REST server.

## Docker Images
* [fduarte42/rpi-phpmyadmin](https://hub.docker.com/r/hypriot/rpi-mysql/)
* [hypriot/rpi-mysql:latest](https://hub.docker.com/r/fduarte42/rpi-phpmyadmin/)
* [hypriot/rpi-node](https://hub.docker.com/r/hypriot/rpi-node/)
* [akkerman/rpi-nginx](https://hub.docker.com/r/akkerman/rpi-nginx/)

### Install Mysql Server

```bash
docker pull hypriot/rpi-mysql:latest
docker run --name mysql-server -e MYSQL_ROOT_PASSWORD=<secret-password> --net="host" --restart always -d -p 3306:3306 hypriot/rpi-mysql:latest
```

### Install Phpmyadmin

```bash
docker pull fduarte42/rpi-phpmyadmin
docker run --name phpmyadmin -d -e PMA_HOST=<Mysql-host-ip> --restart always -p 8090:80 fduarte42/rpi-phpmyadmin
```

### Install Node JS to serve Plapper-be.

Create folder.

```bash
sudo mkdir -p /usr/src/plapper
sudo chown pi:pi /usr/src/plapper
```

Download plapper API REST code to **/usr/src/plapper** folder.
```bash
git clone http://192.168.0.102/dyako/plapper-be.git
```

Build docker image from dockerfile inside the project.
```bash
docker build -t plapper/plapper-be .
```

Run docker container from plapper/plapper-be image.
```bash
docker run --name plapper-be -p 8888:8888 --restart always  plapper/plapper-be:latest
```

# Install Nginx to serve plapper-fe application.

Create folder.

```bash
sudo mkdir -p /usr/src/plapper
sudo chown pi:pi /usr/src/plapper
```

Download plapper Web App code to **/usr/src/plapper** folder.
```bash
git clone http://192.168.0.102/dyako/plapper-fe.git
```

Download image of Nginx.

```bash
docker pull akkerman/rpi-nginx
```

Run Nginx container serving application.

```bash
docker run --name plapper-fe -v /usr/src/plapper/plapper-fe/www:/var/www/html/plapper --restart always -p 9090:80 -d akkerman/rpi-nginx
```


# INSTALL GITLAB

To install gitlab on Raspberry Pi 3 keep these steps.

### Install and configure the necessary dependencies

```bash
sudo apt-get install curl openssh-server ca-certificates postfix apt-transport-https

curl https://packages.gitlab.com/gpg.key | sudo apt-key add -
```

#### Add the GitLab package server and install the package

```bash
sudo curl -sS https://packages.gitlab.com/install/repositories/gitlab/raspberry-pi2/script.deb.sh | sudo bash

sudo apt-get install gitlab-ce
```

###  Configure and start GitLab

```bash
sudo gitlab-ctl reconfigure
```