
function Logger(level) {
    
    this.debugLevel = level;
    this.levels = ['debug', 'error', 'warm', 'success', 'info'];
    this.colors = ['\x1b[35m%s\x1b[0m', '\x1b[31m%s\x1b[0m', '\x1b[33m%s\x1b[0m', '\x1b[32m%s\x1b[0m', '\x1b[36m%s\x1b[0m'];
    
};

Logger.prototype.log = function(level, message) {
    
    var d = new Date, 
            dformat = [ (d.getMonth()+1),
                    d.getDate(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours(),
                    d.getMinutes(),
                    d.getSeconds()].join(':');
    
    if (this.levels.indexOf(level) >= this.levels.indexOf(this.debugLevel) ) {
      if (typeof message !== 'string') {
        message = JSON.stringify(message);
      };
      console.log(this.colors[this.levels.indexOf(level)], '['+level+'] - ['+dformat+'] : '+message);
    }
};

Logger.prototype.debug = function(message) {
    this.log('debug', message);
}

Logger.prototype.error = function(message) {
    this.log('error', message);
}

Logger.prototype.warm = function(message) {
    this.log('warm', message);
}

Logger.prototype.success = function(message) {
    this.log('success', message);
}

Logger.prototype.info = function(message) {
    this.log('info', message);
}

module.exports = new Logger('debug');