var mysql       = require('mysql');

var logger      = require('./Logger.js');
var config      = require('../config.js');

var con = mysql.createConnection({
    host: config.host,
    user: config.database.name,
    password: config.database.password,
    database: config.database.name
});

con.connect(function(err){
  if(err){
    logger.error('Error connecting to Mysql BBDD');
    return;
  }
  
  logger.success('Mysql Connection established');
  return;
});

module.exports = con;