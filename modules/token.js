var jwt         = require('jsonwebtoken');

var logger      = require('./Logger.js');

function Token(app) {

    this.validation = function(req, res, next) {
        var token = req.body.token || req.query.token || req.headers['authorization'];
        logger.warm(req.headers);

        if (token) {
            jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
                if (err)  { return res.json({ err: 1, message: 'Failed to authenticate token.' });    
                } else    { req.decoded = decoded; next(); }
            });

        } else {
            return res.status(403).json({ 
                err: 1, 
                message: 'No token provided.' 
            });
            
        }  
    };
}

module.exports = Token;