FROM hypriot/rpi-node:boron

# Create app directory
RUN mkdir -p /usr/src/plapper/plapper-be
WORKDIR /usr/src/plapper/plapper-be

# Install app dependencies
COPY package.json /usr/src/plapper/plapper-be/
RUN npm install

# Bundle app source
COPY . /usr/src/plapper/plapper-be

EXPOSE 8888
CMD [ "npm", "start" ]