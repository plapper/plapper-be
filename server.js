var logger      = require('./modules/Logger.js');
var config      = require('./config.js');

var express     = require('express');
var bodyParser  = require('body-parser');

var app         = express();
var http        = require('http').Server(app);

var Route       = require('./routes/route.js');

//var apiRoutes = express.Router();
app.set('superSecret', config.jwt.secret); // secret variable


// Server Configuration.
app.set('port', config.port);
app.set('appName', config.app.name);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Allow to connect other hosts to API REST.
// Filter HTTP options that are allowed.
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
 });


var route = Route(app);

// Create the server on the port specified.
http.listen(app.get('port'));
logger.success("Http Server listen on port " + app.get('port'));